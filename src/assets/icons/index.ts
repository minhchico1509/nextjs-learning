import AppLogo from './app-logo.svg';
import CloseIcon from './close.svg';
import DefaultIcon from './default.svg';
import ErrorIcon from './error.svg';

export { AppLogo, CloseIcon, DefaultIcon, ErrorIcon };
